package org.glycoinfo.wurcsfilter.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.wurcsfilter.feature.IFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Filter {

	private static final Logger logger = LoggerFactory.getLogger(Filter.class);

	private String m_strInput;
	private boolean m_bDoInvertMatch;
	private boolean m_bOutputReport;

	private List<IFeature> m_lFeatures;

	public Filter() {
		this.m_strInput = null;
		this.m_bDoInvertMatch = false;
		this.m_bOutputReport = false;

		this.m_lFeatures = new ArrayList<>();
	}

	public Filter setInput(String a_strInput) {
		this.m_strInput = a_strInput;
		return this;
	}

	public Filter setInvertMatch(boolean a_bInvertMatch) {
        this.m_bDoInvertMatch = a_bInvertMatch;
        return this;
	}

	public Filter setOutputReport(boolean a_bOutputReport) {
		this.m_bOutputReport = a_bOutputReport;
		return this;
	}

	public Filter addFeature(IFeature a_feature) {
		this.m_lFeatures.add(a_feature);
        return this;
	}

	public void run() throws IOException {
		Reader in = getReader(this.m_strInput);

		process(in);
	}

	private Reader getReader(String strInput) {
		// Read from stdin if no input
		if ( strInput == null )
			return new InputStreamReader(System.in);

		// Read from file if it can be read
		File fileIn = new File(strInput);
		if ( fileIn.exists() && fileIn.canRead() )
			try {
				logger.info("Read input as file: {}", this.m_strInput);
				return new FileReader(fileIn);
			} catch (FileNotFoundException e) {
				return null;
			}

		// Read input string
		logger.info("Read input as string: {}", this.m_strInput);
		return new StringReader(this.m_strInput);
	}

	private void process(Reader in) {
		BufferedReader reader = new BufferedReader(in);

		reader.lines().forEach( line -> {
			if ( match(line) )
				System.out.println(line);
			if (this.m_bOutputReport)
				printReport();
		});
	}

	private boolean match(String line) {
		if ( line == null )
			return false;

		// Remove index if exist
		String seq = line;
		if ( seq.contains("\t") )
			seq = seq.split("\t")[1];

		boolean isMatched = false;
		for (IFeature feature : this.m_lFeatures) {
			isMatched |= feature.match(seq);
		}
		if (this.m_bDoInvertMatch)
			isMatched = !isMatched;
		return isMatched;
	}

	private void printReport() {
		for (IFeature feature : this.m_lFeatures)
			System.err.println(feature.getReport());
	}
}
