package org.glycoinfo.wurcsfilter.cli;

import java.io.PrintWriter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.wurcsfilter.feature.IFeature;
import org.glycoinfo.wurcsfilter.feature.MatcherFeature;
import org.glycoinfo.wurcsfilter.feature.MatcherFeature.PredefinedType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static final Logger logger = LoggerFactory.getLogger(App.class);

	static final String APP_NAME = "WURCSFilter";
	static final String APP_VERSION = "0.4.2";

	private static final int STATUS_RUNTIME_ERROR  = 1;
	private static final int STATUS_PARSE_ERROR    = 2;

	public static void main(String[] args) {
		new App().run(args);
	}

	public void run(String[] args) {
		CommandLineParser parser = new DefaultParser();

		try {
			CommandLine cmd = parser.parse(options(), args);

			if ( cmd.hasOption("help") ) {
				printUsage();
				return;
			}

			// Check unallowed combinations of options
			if ( cmd.hasOption(OPTION_ALLOW_ALL_MS_LONG)
			  && ( cmd.hasOption(OPTION_MS_PATTERN_LONG) || cmd.hasOption(OPTION_MS_PATTERN_LONG) ) )
				throw new IllegalArgumentException("The options can not be use at the same time:"
						+ "allow-all-ms vs ms/ms-name");

			if ( cmd.hasOption(OPTION_ALLOW_ALL_SUB_LONG)
			  && ( cmd.hasOption(OPTION_SUB_PATTERN_LONG) || cmd.hasOption(OPTION_SUB_PATTERN_LONG) ) )
				throw new IllegalArgumentException("The options can not be use at the same time:"
						+ "allow-all-sub vs sub/sub-name");

			// For features
			IFeature matcher = new MatcherFeature()
					.setMonosaccharidesShort(cmd.getOptionValue(OPTION_MS_PATTERN_NAME_LONG))
					.setMonosaccharidesLong(cmd.getOptionValue(OPTION_MS_PATTERN_LONG))
					.setSubstituentsShort(cmd.getOptionValue(OPTION_SUB_PATTERN_NAME_LONG))
					.setSubstituentsLong(cmd.getOptionValue(OPTION_SUB_PATTERN_LONG))
					.setAllowContainingUndefined(cmd.hasOption(OPTION_ALLOW_UNDEF_LONG))
					.setAllowAllMonosaccharides(cmd.hasOption(OPTION_ALLOW_ALL_MS_LONG))
					.setAllowAllSubstituents(cmd.hasOption(OPTION_ALLOW_ALL_SUB_LONG))
					.setForceAnomerCheck(cmd.hasOption(OPTION_FORCE_ANOMER_LONG))
					.setPredefinedType( PredefinedType.fromString( cmd.getOptionValue(OPTION_PREDEF_TYPE_LONG) ) )
				;

			// For filter
			new Filter()
			.setInput(cmd.getArgList().isEmpty()? null : cmd.getArgList().get(0))
			.setInvertMatch(cmd.hasOption(OPTION_INVERT_MATCH_SHORT))
			.setOutputReport(cmd.hasOption(OPTION_OUTPUT_REPORT_SHORT))
			.addFeature(matcher)
			.run();
		} catch (ParseException e) {
			logger.error("Parse Error:", e);
			printUsage();
			System.exit(STATUS_PARSE_ERROR);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			System.exit(STATUS_RUNTIME_ERROR);
		}
	}

	private void printUsage() {
		String syntax = String
			.format("%s "
				+ " [--%s]"
				+ " [--%s]"
				+ " [--%s <Name1,Name2,...>]"
				+ " [--%s <Name1|ResidueCode1|Description1,Name2|ResidueCode2|Description2,...>]"
				+ " [--%s <Name1,Name2,...>]"
				+ " [--%s <Name1|MAPCode1|Description1,Name2|MAPCode2|Description2,...>]"
				+ " [--%s]"
				+ " [--%s]"
				+ " [--%s]"
				+ " [--%s]"
				+ " [--%s]"
				+ " [--%s <TYPE>]"
				+ "[<INPUT FORMAT STRING>]",
				APP_NAME,
				OPTION_INVERT_MATCH_LONG,
				OPTION_OUTPUT_REPORT_LONG,
				OPTION_MS_PATTERN_NAME_LONG,
				OPTION_MS_PATTERN_LONG,
				OPTION_SUB_PATTERN_NAME_LONG,
				OPTION_SUB_PATTERN_LONG,
				OPTION_ALLOW_UNDEF_LONG,
				OPTION_ALLOW_ALL_MS_LONG,
				OPTION_ALLOW_ALL_SUB_LONG,
				OPTION_FORCE_ANOMER_LONG,
				OPTION_PREDEF_TYPE_LONG
			);
		String header = "\nOptions:";
		String footer = String.format("\nVersion: %s", APP_VERSION);

		HelpFormatter hf = new HelpFormatter();

		hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
			header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
	}

	// For common
	private static final String OPTION_INVERT_MATCH_SHORT = "v";
	private static final String OPTION_INVERT_MATCH_LONG = "invert-match";
	private static final String OPTION_OUTPUT_REPORT_SHORT = "r";
	private static final String OPTION_OUTPUT_REPORT_LONG = "output-report";

	// For matcher feature
	private static final String OPTION_MS_PATTERN_NAME_LONG = "ms-name";
	private static final String OPTION_MS_PATTERN_LONG = "ms";
	private static final String OPTION_SUB_PATTERN_NAME_LONG = "sub-name";
	private static final String OPTION_SUB_PATTERN_LONG = "sub";
	private static final String OPTION_ALLOW_UNDEF_LONG = "allow-undef";
	private static final String OPTION_ALLOW_ALL_MS_LONG = "allow-all-ms";
	private static final String OPTION_ALLOW_ALL_SUB_LONG = "allow-all-sub";
	private static final String OPTION_FORCE_ANOMER_LONG = "force-anomer-match";

	private static final String OPTION_PREDEF_TYPE_LONG = "predef-type";
	private static final String OPTION_PREDEF_TYPE_SHORT = "t";
	private static final String[] ARGUMENTS_PREDEF_TYPE = MatcherFeature.PredefinedType.getNames();


	private Options options() {
		Options options = new Options();

		options.addOption(Option.builder("h")
				.longOpt("help")
				.desc("Show usage help")
				.build()
			);

		// For common
		options.addOption(Option.builder(OPTION_INVERT_MATCH_SHORT)
				.longOpt(OPTION_INVERT_MATCH_LONG)
				.desc("Invert match")
				.build()
			);

		options.addOption(Option.builder(OPTION_OUTPUT_REPORT_SHORT)
				.longOpt(OPTION_OUTPUT_REPORT_LONG)
				.desc("Output report")
				.build()
			);

		// For matcher feature
		options.addOption(Option.builder()
				.longOpt(OPTION_MS_PATTERN_NAME_LONG)
				.desc("Set monosaccharide pattern as trivial name defined in SNFG")
				.hasArg()
				.argName("Name1,Name2,...")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_MS_PATTERN_LONG)
				.desc("Set monosaccharide pattern as WURCS representation")
				.hasArg()
				.argName("Name1|ResidueCode1|Description1,Name2|ResidueCode2|Description2,...")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_SUB_PATTERN_NAME_LONG)
				.desc("Set substituent pattern as trivial name defined in SNFG")
				.hasArg()
				.argName("Name1,Name2,...")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_SUB_PATTERN_LONG)
				.desc("Set substituent pattern as WURCS representation")
				.hasArg()
				.argName("Name1|MAPCode1|Description1,Name2|MAPCode2|Description2,...")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_ALLOW_UNDEF_LONG)
				.desc("Allow undefined pattern is contained")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_ALLOW_ALL_MS_LONG)
				.desc("Allow all monosaccharides")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_ALLOW_ALL_SUB_LONG)
				.desc("Allow all substituents")
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_FORCE_ANOMER_LONG)
				.desc("Force anomer check")
				.build()
			);

		options.addOption(Option.builder(OPTION_PREDEF_TYPE_SHORT)
				.longOpt(OPTION_PREDEF_TYPE_LONG)
				.desc("Select predefined patterns to allow (default is \"snfg\", \"no\" for no predefined type)")
				.hasArg()
				.argName("TYPE=[" + String.join("|", ARGUMENTS_PREDEF_TYPE) + "]")
				.build()
			);

		return options;
	}

}
