package org.glycoinfo.wurcsfilter.feature;

public interface IFeature {
	public boolean match(String a_strWURCS);
	public String getReport();
}
