package org.glycoinfo.wurcsfilter.feature;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.wurcsframework.patternmatching.WURCSMatcher;
import org.glycoinfo.wurcsframework.patternmatching.pattern.PatternDictionary;
import org.glycoinfo.wurcsframework.patternmatching.util.DictionaryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MatcherFeature implements IFeature {

	public static enum PredefinedType {
		SNFG, STORM, NONE;

		public static String[] getNames() {
            String[] names = new String[PredefinedType.values().length];
            for ( int i=0; i<PredefinedType.values().length; i++ )
                names[i] = PredefinedType.values()[i].name().toLowerCase();
            return names;
		}

		public static PredefinedType fromString(String a_strType) {
			for ( PredefinedType type : PredefinedType.values() )
				if ( type.name().equalsIgnoreCase(a_strType) )
					return type;
            return null;
		}
	}

	private static final Logger logger = LoggerFactory.getLogger(MatcherFeature.class);

	// Options
	private String m_strMonosaccharidesShort;
	private String m_strMonosaccharidesLong;
	private String m_strSubstituentsShort;
	private String m_strSubstituentsLong;
	private boolean m_bAllowContainingUndefined;
	private boolean m_bAllowAllMonosaccharides;
	private boolean m_bAllowAllSubstituents;
	private boolean m_bForceAnomerCheck;
	private PredefinedType m_predefType;

	private StringBuilder m_sbReport;

	public MatcherFeature() {
		this.m_strMonosaccharidesShort = null;
		this.m_strMonosaccharidesLong = null;
		this.m_strSubstituentsShort = null;
		this.m_strSubstituentsLong = null;
		this.m_bAllowContainingUndefined = false;
		this.m_bAllowAllMonosaccharides = false;
		this.m_bAllowAllSubstituents = false;
		this.m_bForceAnomerCheck = false;

		this.m_predefType = PredefinedType.SNFG;
	}

	public MatcherFeature setMonosaccharidesShort(String a_strMono) {
		this.m_strMonosaccharidesShort = a_strMono;
		return this;
	}

	public MatcherFeature setMonosaccharidesLong(String a_strMono) {
		this.m_strMonosaccharidesLong = a_strMono;
		return this;
	}

	public MatcherFeature setSubstituentsShort(String a_strSub) {
		this.m_strSubstituentsShort = a_strSub;
		return this;
	}

	public MatcherFeature setSubstituentsLong(String a_strSub) {
		this.m_strSubstituentsLong = a_strSub;
		return this;
	}

	public MatcherFeature setAllowContainingUndefined(boolean a_bAllowContainingUndefined) {
		this.m_bAllowContainingUndefined = a_bAllowContainingUndefined;
		return this;
	}

	public MatcherFeature setAllowAllMonosaccharides(boolean a_bAllowAllMonosaccharides) {
		this.m_bAllowAllMonosaccharides = a_bAllowAllMonosaccharides;
		return this;
	}

	public MatcherFeature setAllowAllSubstituents(boolean a_bAllowAllSubstituents) {
		this.m_bAllowAllSubstituents = a_bAllowAllSubstituents;
		return this;
	}

	public MatcherFeature setForceAnomerCheck(boolean a_bForceAnomerCheck) {
		this.m_bForceAnomerCheck = a_bForceAnomerCheck;
		return this;
	}

	public MatcherFeature setPredefinedType(PredefinedType a_predefType) {
		this.m_predefType = a_predefType;
		return this;
	}

	@Override
	public String getReport() {
        return this.m_sbReport.toString();
	}

	@Override
	public boolean match(String a_strWURCS) {
		WURCSMatcher matcher = buildMatcher();
		try {
			matcher.start(a_strWURCS);

			boolean isMatch = false;
			if ( this.m_bAllowAllMonosaccharides )
				isMatch = !matcher.getReport().hasUnknownMAPPattern();
			else if ( this.m_bAllowAllSubstituents )
				isMatch = !matcher.getReport().hasUnknownMSPattern();
			else {
				isMatch = matcher.getReport().hasMatchedPattern();
				if ( !this.m_bAllowContainingUndefined )
	                isMatch &= !matcher.getReport().hasUnknownPattern();
			}


			report(isMatch, matcher.getReport().getSummary());

			return isMatch;
		} catch (WURCSFormatException e) {
			logger.error("Error in WURCSMatcher", e);
			return false;
		}
	}

	private WURCSMatcher buildMatcher() {
		PatternDictionary dict = new PatternDictionary();

		// Set predefined patterns
		switch (this.m_predefType) {
		case NONE:
			// Do not set predefined patterns
			break;
		case STORM:
			DictionaryUtil.setSTORMPatternAll(dict);
			break;
		case SNFG:
		default:
			DictionaryUtil.setSNFGPatternAll(dict);
			break;
		}

		// Set monosaccharides and substituents for filter
		setMonosaccharidesForFilter(this.m_strMonosaccharidesShort, dict);
		setMonosaccharidesForFilter(this.m_strMonosaccharidesLong, dict);
		setSubstituentsForFilter(this.m_strSubstituentsShort, dict);
		setSubstituentsForFilter(this.m_strSubstituentsLong, dict);

		WURCSMatcher matcher = new WURCSMatcher(dict);
		// Set anomer check
		matcher.setDoCheckAnomer(this.m_bForceAnomerCheck);
		// Set fuzzy match for STORM
		matcher.setDoFuzzyMatch( this.m_predefType == PredefinedType.STORM );

		return matcher;
	}

	private void setMonosaccharidesForFilter(String a_strMono, PatternDictionary a_dict) {
		if ( a_strMono == null )
			return;

		// For monosaccharide short
		if ( !a_strMono.contains("|") ) {
			DictionaryUtil.addSNFGPatternForMSNames(a_dict, a_strMono.split(","));
			return;
		}

		// For monosaccharide long
		for ( String ms : a_strMono.split(",") ) {
			if ( ms.isEmpty() ) continue;
			String[] msLong = ms.split("\\|");
			a_dict.addMSPattern(msLong[0], msLong[1], msLong[2]);
		}

	}

	private void setSubstituentsForFilter(String a_strSubst, PatternDictionary a_dict) {
		if ( a_strSubst == null )
            return;

		if ( !a_strSubst.contains("|") ) {
			DictionaryUtil.addSNFGPatternForMAPNames(a_dict, a_strSubst.split(","));
			return;
		}

		for ( String map : a_strSubst.split(",") ) {
			if ( map.isEmpty() ) continue;
			String[] mapLong = map.split("\\|");
			a_dict.addMAPPattern(mapLong[0], mapLong[1], mapLong[2]);
		}
	}

	private void report(boolean a_bIsMatched, String a_strReport) {
		this.m_sbReport = new StringBuilder("WURCSFilterReport: ");
		this.m_sbReport.append(a_bIsMatched ? "Matched" : "Not matched");
		this.m_sbReport.append(": ");
		this.m_sbReport.append(a_strReport);
	}
}
