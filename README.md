# WURCS Filter Utility

WURCS Filter Utility is a software for filtering WURCSs with specific conditions.

"WURCS" stands for Web3.0 Unique Representation of Carbohydrate Structure

* Matsubara, M. _et al._, _J. Chem. Inf. Model._, 57, 632-37 (2017)


## Features
* Filter input WURCS with specific conditions such as:
  * only monosaccharides and substituents defined in the SNFG (Symbol Nomenclature for Glycans)


## Requirements
* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher


## Build

    $ mvn clean compile assembly:single


## Usage

    $ java - jar WURCSFilter.jar  [--invert-match] [--output-report] [--ms-name <Name1,Name2,...>] [--ms <Name1|ResidueCode1|Description1,Name2|ResidueCode2|Description2,...>] [--sub-name <Name1,Name2,...>] [--sub <Name1|MAPCode1|Description1,Name2|MAPCode2|Description2,...>] [--allow-undef] [--force-anomer-match] [--predef-type <TYPE>] [<INPUT FORMAT STRING>]

### Options

| option | argument | description |
| ------ | -------- | ----------- |
| -v, --invert-match  |  | Invert match  |
| -r, --output-report |  | Output report |
|     --ms-name       | Name1,Name2,... | Set monosaccharide pattern as trivial name defined in SNFG |
|     --ms            | Name1|ResidueCode1|Description1,Name2|ResidueCode2|Description2,... | Set monosaccharide pattern as ResidueCode (WURCS representation) |
|     --sub-name      | Name1,Name2,... | Set substituent pattern as trivial name defined in SNFG |
|     --sub           | Name1|MAPCode1|Description1,Name2|MAPCode2|Description2,... | Set substituent pattern as MAP code (WURCS representation) |
|     --allow-undef   |  | Allow undefined pattern is contained |
|     --force-anomer-match |  | Consider anomer when checking monosaccharide pattern (no anomer check as default) |
| -t, --predef-type    | TYPE=[snfg\|storm\|none] | Select predefined patterns to allow (default is \"snfg\", \"none\" for using no predefined type) |

### Example

Give input string as an argument

    $ java -jar WURCSFilter.jar "WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/"

Title for the WURCS can also be specified in input string  
(The title must be head of the string and separated with tab character)

    $ java -jar WURCSFilter.jar "Title    WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/"

Give input string as standard input

    $ echo "Title    WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/" | java -jar WURCSFilter.jar

Give input file of WURCS list as an argument

    $ java -jar WURCSFilter.jar "path/to/WURCS.lst"

Give input file contents as standard input

    $ cat "path/to/WURCS.lst" | java -jar WURCSFilter.jar

## Filtering WURCS with SNFG

As default, input WURCS(s) are filtered out when they have structures not defined in
 [SNFG](https://www.ncbi.nlm.nih.gov/glycans/snfg.html).  
 Some substituents to be used for filter are not defined in SNFG.

### SNFG Monosaccharides
| Name     | WURCS (ResidueCode)                        | Common name                                  |
|----------|--------------------------------------------|----------------------------------------------|
| Hex      | `axxxxh-1x_1-5`                            | Hexose                                       |
| Glc      | `a2122h-1x_1-5`                            | D-Glucose                                    |
| Man      | `a1122h-1x_1-5`                            | D-Mannose                                    |
| Gal      | `a2112h-1x_1-5`                            | D-Galactose                                  |
| Gul      | `a2212h-1x_1-5`                            | D-Gulose                                     |
| Alt      | `a2111h-1x_1-5`                            | L-Altrose                                    |
| All      | `a2222h-1x_1-5`                            | D-Allose                                     |
| Tal      | `a1112h-1x_1-5`                            | D-Talose                                     |
| Ido      | `a2121h-1x_1-5`                            | L-Idose                                      |
|          |                                            |                                              |
| HexNAc   | `axxxxh-1x_1-5_2*NCC/3=O`                  | HexNAc                                       |
| GlcNAc   | `a2122h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-glucosamine                       |
| ManNAc   | `a1122h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-mannosamine                       |
| GalNAc   | `a2112h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-galactosamine                     |
| GulNAc   | `a2212h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-gulosamine                        |
| AltNAc   | `a2111h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-L-altrosamine                       |
| AllNAc   | `a2222h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-allosamine                        |
| TalNAc   | `a1112h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-talosamine                        |
| IdoNAc   | `a2121h-1x_1-5_2*NCC/3=O`                  | N-Acetyl-L-idosamine                         |
|          |                                            |                                              |
| HexN     | `axxxxh-1x_1-5_2*N`                        | Hexosamine                                   |
| GlcN     | `a2122h-1x_1-5_2*N`                        | D-Glucosamine                                |
| ManN     | `a1122h-1x_1-5_2*N`                        | D-Mannosamine                                |
| GalN     | `a2112h-1x_1-5_2*N`                        | D-Galactosamine                              |
| GulN     | `a2212h-1x_1-5_2*N`                        | D-Gulosamine                                 |
| AltN     | `a2111h-1x_1-5_2*N`                        | L-Altrosamine                                |
| AllN     | `a2222h-1x_1-5_2*N`                        | D-Allosamine                                 |
| TalN     | `a1112h-1x_1-5_2*N`                        | D-Talosamine                                 |
| IdoN     | `a2121h-1x_1-5_2*N`                        | L-Idosamine                                  |
|          |                                            |                                              |
| HexA     | `axxxxA-1x_1-5`                            | Hexuronate                                   |
| GlcA     | `a2122A-1x_1-5`                            | D-Glucuronic acid                            |
| ManA     | `a1122A-1x_1-5`                            | D-Mannuronic acid                            |
| GalA     | `a2112A-1x_1-5`                            | D-Galacturonic acid                          |
| GulA     | `a2212A-1x_1-5`                            | D-Guluonic acid                              |
| AltA     | `a2111A-1x_1-5`                            | L-Altruronic acid                            |
| AllA     | `a2222A-1x_1-5`                            | D-Alluronic acid                             |
| TalA     | `a1112A-1x_1-5`                            | D-Taluronic acid                             |
| IdoA     | `a2121A-1x_1-5`                            | L-Iduronic acid                              |
|          |                                            |                                              |
| dHex     | `axxxxm-1x_1-5`                            | Deoxyhexose                                  |
| Qui      | `a2122m-1x_1-5`                            | D-Quinovose                                  |
| Rha      | `a2211m-1x_1-5`                            | L-Rhamnose                                   |
| 6dGul    | `a2212m-1x_1-5`                            | 6-Deoxy-D-gulose                             |
| 6dAlt    | `a2111m-1x_1-5`                            | 6-Deoxy-L-altrose                            |
| 6dTal    | `a1112m-1x_1-5`                            | 6-Deoxy-D-talose                             |
| Fuc      | `a1221m-1x_1-5`                            | L-Fucose                                     |
|          |                                            |                                              |
| dHexNAc  | `axxxxm-1x_1-5_2*NCC/3=O`                  | DeoxyhexNAc                                  |
| QuiNAc   | `a2122m-1x_1-5_2*NCC/3=O`                  | N-Acetyl-D-quinovosamine                     |
| RhaNAc   | `a2211m-1x_1-5_2*NCC/3=O`                  | N-Acetyl-L-rhamnosamine                      |
| 6dAltNAc | `a2111m-1x_1-5_2*NCC/3=O`                  | N-Acetyl-6-deoxy-L-altrosamine               |
| 6dTalNAc | `a1112m-1x_1-5_2*NCC/3=O`                  | N-Acetyl-6-deoxy-D-talosamine                |
| FucNAc   | `a1221m-1x_1-5_2*NCC/3=O`                  | N-Acetyl-L-fucosamine                        |
|          |                                            |                                              |
| ddHex    | `adxxxm-1x_1-5`                            | Di-deoxyhexose                               |
| Oli      | `ad122m-1x_1-5`                            | Olivose                                      |
| Tyv      | `a1d22m-1x_1-5`                            | Tyvelose                                     |
| Abe      | `a2d12m-1x_1-5`                            | Abequose                                     |
| Par      | `a2d22m-1x_1-5`                            | Paratose                                     |
| Dig      | `ad222m-1x_1-5`                            | Digitoxose                                   |
| Col      | `a1d21m-1x_1-5`                            | Colitose                                     |
|          |                                            |                                              |
| Pen      | `axxxh-1x_1-5`                             | Pentose                                      |
| Ara      | `a211h-1x_1-5`                             | L-Arabinose                                  |
| Lyx      | `a112h-1x_1-5`                             | D-Lyxose                                     |
| Xyl      | `a212h-1x_1-5`                             | D-Xylose                                     |
| Rib      | `a222h-1x_1-5`                             | D-Ribose                                     |
|          |                                            |                                              |
| dNulO    | `Aadxxxxxh-2x_2-6`                         | 3-deoxy-nonulosonic acids                    |
| Kdn      | `Aad21122h-2x_2-6`                         | 3-Deoxy-D-glycero-D-galacto-nonulosonic Acid |
| Neu5Ac   | `Aad21122h-2x_2-6_5*NCC/3=O`               | N-Acetylneuraminic acid                      |
| Neu5Gc   | `Aad21122h-2x_2-6_5*NCCO/3=O`              | N-Glycolylneuraminic acid                    |
| Neu      | `Aad21122h-2x_2-6_5*N`                     | Neuraminic acid                              |
|          |                                            |                                              |
| ddNulO   | `Aadxxxxxm-2x_2-6_5*N_7*N`                 | 3,9-deoxy-nonulosonic acids                  |
| Pse      | `Aad22111m-2x_2-6_5*N_7*N`                 | Pseudaminic acid                             |
| Leg      | `Aad21122m-2x_2-6_5*N_7*N`                 | Legionaminic acid                            |
| Aci      | `Aad21111m-2x_2-6_5*N_7*N`                 | Acinetaminic acid                            |
| 4eLeg    | `Aad11122m-2x_2-6_5*N_7*N`                 | 4-Epilegionaminic acid                       |
|          |                                            |                                              |
| Bac      | `a2122m-1x_1-5_2*N_4*N`                    | Bacillosamine                                |
| LDmanHep | `a11221h-1x_1-5`                           | L-glycero-D-manno-Heptose                    |
| Kdo      | `Aad1122h-2x_2-6`                          | 3-Deoxy-D-manno-octulosonic acid             |
| Dha      | `Aad112A-2x_2-6`                           | 3-Deoxy-D-lyxo-heptulosaric acid             |
| DDmanHep | `a11222h-1x_1-5`                           | D-glycero-D-manno-Heptose                    |
| MurNAc   | `a2122h-1x_1-5_2*NCC/3=O_3*OC^RCO/4=O/3C`  | N-Acetylmuramic acid                         |
| MurNGc   | `a2122h-1x_1-5_2*NCCO/3=O_3*OC^RCO/4=O/3C` | N-Glycolylmuramic acid                       |
| Mur      | `a2122h-1x_1-5_2*N_3*OC^RCO/4=O/3C`        | Muramic acid                                 |
|          |                                            |                                              |
| L-Api    | `a15h-1x_1-4_3*CO`                         | L-Apiose                                     |
| D-Api    | `a26h-1x_1-4_3*CO`                         | D-Apiose (Not in SNFG, but D-Api is more findable)                                     |
| Fru      | `ha122h-2x_2-6`                            | D-Fructose                                   |
| Tag      | `ha112h-2x_2-6`                            | D-Tagatose                                   |
| Sor      | `ha121h-2x_2-6`                            | L-Sorbose                                    |
| Psi      | `ha222h-2x_2-6`                            | D-Psicose                                    |

### SNFG Substituents
| Common name                  | abbr.     | MAP                            |
|------------------------------|-----------|--------------------------------|
| O-acetyl                     | OAc       | `*OCC/3=O`                     |
| O-D-alanyl                   | OAla      | `*OCC^RC/4N/3=O`               |
| O-(N-acetyl-D-alanyl)        | OAla2Ac   | `*OCC^RNCC/6=O/4C/3=O`         |
| N-acetimidoyl                | Am        | `*N=^XCC/3N`                   |
| N-acetimidoyl-N-methyl       | AmMe      | `*N=^XCC/3N/2C`                |
| N-(N,N-dimethyl-acetimidoyl) | AmMe2     | `*N=^XCNC/4C/3C`               |
| O-formyl                     | OFo       | `*OC=O`                        |
| O-glycoryl                   | OGc       | `*OCCO/3=O`                    |
| O-(N-acetyl-glutaminyl)      | OGln2Ac   | `*OCC^XNCC/6=O/4CCCN/11=O/3=O` |
| O-(N-methyl-5-glutamyl)      | O5Glu2Me  | `*OCCCC^XCN/7=O/6NC/3=O`       |
| O-glycyl                     | OGly      | `*OCCN/3=O`                    |
| O-glyceryl                   | OGr       | `*OCC^XCO/4O/3=O`              |
| O-(2,3-di-O-methyl-glycaryl) | OGr2,3Me2 | `*OCC^XCOC/4OC/3=O`            |
| O-4-hydroxybutyryl           | O4Hb      | `*OCCCCO/3=O`                  |
| O-(3,4-dihydroxybutyryl)     | O3,4Hb    | `*OCCC^XCO/5O/3=O`             |
| O-(R)-3-hydroxybutyryl       | O3RHb     | `*OCCC^RC/5O/3=O`              |
| O-(S)-3-hydroxybutyryl       | O3SHb     | `*OCCC^SC/5O/3=O`              |
| O-lactyl                     | OLt       | `*OCC^XC/4O/3=O`               |
| O-methyl                     | OMe       | `*OC`                          |
| amino                        | N         | `*N`                           |
| N-acetyl                     | NAc       | `*NCC/3=O`                     |
| N-glycoryl                   | NGc       | `*NCCO/3=O`                    |
| N-sulfate                    | NS        | `*NSO/3=O/3=O`                 |
| phosphate                    | P         | `*OPO/3O/3=O`                  |
| O-pyruvyl                    | OPy       | `*OCCC/4=O/3=O`                |
| 1-carboxyethylidene          | Pyr       | `*OC^XO*/3CO/6=O/3C`           |
| sulfate                      | S         | `*OSO/3=O/3=O`                 |
| tauryl                       | Tau       | `*NCCCSO/6=O/6=O`              |

- Listed in SNFG web site (Table 3)
- Substituents with O- prefix in this list are originally defined as the structures
 without O- in SNFG
- N-glycoryl and N-sulfate are not listed in the SNFG

### Substituents not in SNFG
| Common name                  | abbr.     | MAP                            |
|------------------------------|-----------|--------------------------------|
| O-methoxy             | OMeOH   | `*OCO`                          |
| O-ethyl               | OEt     | `*OCC`                          |
| O-aminoethyl          | OEtN    | `*OCCN`                         |
| O-hydroxyethyl        | OEtOH   | `*OCCO`                         |
| O-methoxyethyl        | OEtOMe  | `*OCCOC`                        |
| O-propyl              | OPr     | `*OCCC`                         |
| N-methyl              | NMe     | `*NC`                           |
| N-dimethyl            | NMe2    | `*NC/2C`                        |
| N-ethyl               | NEt     | `*NCC`                          |
| N-formyl              | NFo     | `*NC=O`                         |
| thio                  | SH      | `*S`                            |
| S-methyl              | SMe     | `*SC`                           |
| S-ethyl               | SEt     | `*SCC`                          |
| C-sulfate             | CS      | `*SO/3=O/3=O`                   |
| O-(1R)-1-carboxyethyl | OREtA   | `*OC^RCO/4=O/3C`                |
| pyrophosphate         | PP      | `*OPOPO/5O/5=O/3O/3=O`          |
| triphosphate          | PPP     | `*OPOPOPO/7O/7=O/5O/5=O/3O/3=O` |
| fluolo                | F       | `*F`                            |
| bromo                 | Br      | `*Br`                           |
| iodo                  | I       | `*I`                            |	

## Filtering WURCS with custom condition

### Specifying monosaccharide names defined in SNFG
Give input file of WURCS list as an argument and only allow Glc and Man. The option "--no-predef" is required not to use the other predefined SNFG pattarns.

    $ java -jar WURCSFilter.jar --predef-type "no" --ms-name "Glc,Man" "path/to/WURCS.lst"

The results are following:
- Input (path/to/WURCS.lst):
```
Glc    WURCS=2.0/1,1,0/[a2122h-1x_1-5]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Gal    WURCS=2.0/1,1,0/[a2112h-1x_1-5]/1/
Man(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5]/1-2/a4-b1
Gal(?4)Man    WURCS=2.0/2,2,1/[a1122h-1x_1-5][a2112h-1x_1-5]/1-2/a4-b1
Glc(?4)Gal    WURCS=2.0/2,2,1/[a2112h-1x_1-5][a2122h-1x_1-5]/1-2/a4-b1
```

- Output (stdout):
```
Glc    WURCS=2.0/1,1,0/[a2122h-1x_1-5]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Man(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5]/1-2/a4-b1
```

The option "--allow-undef" allows WURCS which contains undefined patterns, but at least one feature (monosaccharide or substituent pattern) must be matched.

    $ java -jar WURCSFilter.jar --predef-type "no" --allow-undef --ms-name "Glc,Man" "path/to/WURCS.lst"

The results are following:
- Input (path/to/WURCS.lst):
```
Glc    WURCS=2.0/1,1,0/[a2122h-1x_1-5]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Gal    WURCS=2.0/1,1,0/[a2112h-1x_1-5]/1/
Man(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5]/1-2/a4-b1
Gal(?4)Man    WURCS=2.0/2,2,1/[a1122h-1x_1-5][a2112h-1x_1-5]/1-2/a4-b1
Glc(?4)Gal    WURCS=2.0/2,2,1/[a2112h-1x_1-5][a2122h-1x_1-5]/1-2/a4-b1
```

- Output (stdout):
```
Glc    WURCS=2.0/1,1,0/[a2122h-1x_1-5]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Man(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5]/1-2/a4-b1
Gal(?4)Man    WURCS=2.0/2,2,1/[a1122h-1x_1-5][a2112h-1x_1-5]/1-2/a4-b1
Glc(?4)Gal    WURCS=2.0/2,2,1/[a2112h-1x_1-5][a2122h-1x_1-5]/1-2/a4-b1
```

### Specifying substituents with WURCS representation (MAP code)
Give input file of WURCS list as an argument and only allow Glc, Man and Ac. Ac is defined in SNFG but at here it specified as MAP code.

    $ java -jar WURCSFilter.jar --predef-type "no" --ms-name "Glc,Man" --sub "Ac|*OCC/3=O|O-acetyl" "path/to/WURCS.lst"

The results are following:
- Input (path/to/WURCS.lst):
```
Glc6Ac    WURCS=2.0/1,1,0/[a2122h-1x_1-5_6*OCC/3=O]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Gal4Ac    WURCS=2.0/1,1,0/[a2112h-1x_1-5_4*OCC/3=O]/1/
Man3Ac(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5_3*OCC/3=O]/1-2/a4-b1
Gal(?4)Man6Ac    WURCS=2.0/2,2,1/[a1122h-1x_1-5_6*OCC/3=O][a2112h-1x_1-5]/1-2/a4-b1
Glc(?4)Gal    WURCS=2.0/2,2,1/[a2112h-1x_1-5][a2122h-1x_1-5]/1-2/a4-b1
Glc2Ac(?4)Gal2Ac    WURCS=2.0/2,2,1/[a2112h-1x_1-5_2*OCC/3=O][a2122h-1x_1-5_2*OCC/3=O]/1-2/a4-b1
```

- Output (stdout):
```
Glc6Ac    WURCS=2.0/1,1,0/[a2122h-1x_1-5_6*OCC/3=O]/1/
Man    WURCS=2.0/1,1,0/[a1122h-1x_1-5]/1/
Man3Ac(?4)Glc    WURCS=2.0/2,2,1/[a2122h-1x_1-5][a1122h-1x_1-5_3*OCC/3=O]/1-2/a4-b1
```